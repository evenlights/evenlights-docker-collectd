Collectd Image
==============
* Source: [Butbucket][1]
* Size: 307MB
* System: Ubuntu 18.04

The image is configured for the UTC timezone. 

Note it doesn't provide "out-of-the-box" functionality like writing to a 
graphite server using environment variables. Though, an example configuration 
and docker-compose files are provided below as well as image extension 
suggestions.


Packages
--------
* [collectd][2]
* [docker-collectd-plugin][3]
* [envtpl][4]


Using this image
----------------

Running this image is pretty straightforward and only needs 2 volumes: 

* `collectd.conf`

* you local `docker.sock` needs to be mounted if you are planning on running 
  `docker-collectd-plugin`

Here is an example `docker-compose` configuration:

```docker
version: '3.3'


services:
  collectd:
    privileged: true
    image: evenlights/collectd:latest

    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./collectd.conf:/etc/collectd/collectd.conf
```

And an example for `collectd.conf` with `docker-collectd-plugin` enabled and 
`graphite` output:

```shell script
Hostname "{{ HOST_NAME }}"

TypesDB "/usr/share/collectd/types.db" "/usr/share/collectd/docker-collectd-plugin/dockerplugin.db"

FQDNLookup false
Interval "10"
Timeout 2
ReadThreads 5

LoadPlugin cpu
LoadPlugin df
LoadPlugin load
LoadPlugin memory
LoadPlugin disk
LoadPlugin interface
LoadPlugin uptime
LoadPlugin swap
LoadPlugin python
LoadPlugin write_graphite

<Plugin cpu>
  ReportByCpu "false"
</Plugin>

<Plugin df>
  # expose host's mounts into container using -v /:/host:ro  (location inside container does not matter much)
  # ignore rootfs; else, the root file-system would appear twice, causing
  # one of the updates to fail and spam the log
  FSType rootfs
  # ignore the usual virtual / temporary file-systems
  FSType sysfs
  FSType proc
  FSType devtmpfs
  FSType devpts
  FSType tmpfs
  FSType fusectl
  FSType cgroup
  FSType overlay
  FSType debugfs
  FSType pstore
  FSType securityfs
  FSType hugetlbfs
  FSType squashfs
  FSType mqueue
  MountPoint "/etc/resolv.conf"
  MountPoint "/etc/hostname"
  MountPoint "/etc/hosts"
  IgnoreSelected true
  ReportByDevice false
  ReportReserved true
  ReportInodes true
</Plugin>

<Plugin "disk">
  Disk "/^(([hs]|xv)d[a-z][a-z]?[0-9]*|nvme[0-9]+n1|mapper\/.*|dm-[0-9]+)$/"
  IgnoreSelected false
</Plugin>


<Plugin interface>
  Interface "lo"
  Interface "/^veth.*/"
  Interface "/^docker.*/"
  IgnoreSelected true
</Plugin>

<Plugin python>
  ModulePath "/usr/share/collectd/docker-collectd-plugin"
  Import "dockerplugin"

  <Module dockerplugin>
    BaseURL "unix://var/run/docker.sock"
    Timeout 3
    CpuQuotaPercent true
    CpuSharesPercent true
  </Module>
</Plugin>

<Plugin "write_graphite">
 <Carbon>
   Host "{{ GRAPHITE_HOST }}"
   Port "2003"
   Prefix "collectd."
   EscapeCharacter "_"
   SeparateInstances true
   StoreRates true
   AlwaysAppendDS false
 </Carbon>
</Plugin>
```

Container internals 
-------------------

* `docker-collectd-plugin` resides in 
  `/usr/share/collectd/docker-collectd-plugin/` as suggested by the [official
  documentation][1]


Extending the image
-------------------

You might want to have templated or fixed configuration. You can do it like 
follows:

```dockerfile
FROM evenlights/collectd:latest

ADD collectd.conf.tpl /etc/collectd/collectd.conf.tpl
    
RUN envtpl /etc/collectd/collectd.conf.tpl && collectd -f
```

`envtpl` will fill in values like `{{ GRAPHITE_HOST }}` with respective 
environment variables. In this way you can parametrize a file included in the 
image.


Release flow
------------

Create a release branch, desirably using a *flow plugin.

Build the image:

```shell script
make build
```

Bump the version:

```shell script
bumpversion {major|minor|patch}
```

Tag the image:

```shell script
make tag
```

Login tp your repo, note that this will create file under 
`$HOME/.docker/config.json`:

```shell script
make login
```

Push the image:

    $ make push

Update ``HISTORY.md`` describing changes that this release introduces.

Publish the branch.

Create a pull request into `default`.

Finish the release using *flow.


Credits
-------

* [Andreas Jansson](mailto:andreas@jansson.me.uk) 

* [SignalFX](https://www.signalfx.com/)


[1]: https://bitbucket.org/evenlights/evenlights-docker-collectd/src
[2]: https://collectd.org/documentation.shtml
[3]: https://github.com/signalfx/docker-collectd-plugin
[4]: https://github.com/andreasjansson/envtpl
