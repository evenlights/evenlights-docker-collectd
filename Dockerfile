FROM ubuntu:18.04

MAINTAINER Evenlights Studio <development@evenlights.com>

ENV IMAGE_VERSION 1.0.0
ENV DEBIAN_FRONTEND noninteractive

RUN set -ex \
    && pythonDeps=" \
        python \
        python-dev \
        python-pip \
        python-setuptools \
        python-wheel \
    " \
    \
    && apt-get update \
    \
    && apt-get install -y --no-install-recommends $pythonDeps \
    && apt-get install -y --no-install-recommends git collectd curl \
    \
    && rm -rf /var/lib/apt/lists/*

# https://github.com/docker/docker-py/issues/1502
RUN cd /usr/share/collectd \
    && git clone https://github.com/signalfx/docker-collectd-plugin \
    && cd docker-collectd-plugin \
    && pip install -r requirements.txt \
    && pip install setuptools envtpl \
    && cp -r /usr/local/lib/python2.7/dist-packages/backports/ssl_match_hostname/ \
        /usr/lib/python2.7/dist-packages/backports

CMD collectd -f
